﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{

    public float fireRate = 0.5f;
    float cooldown = 0;

    void Update()
    {
        cooldown -= Time.deltaTime;

        if(Input.GetButton("Fire1"))
        {
            Fire();
        }
    }

    void Fire()
    {
        if (cooldown > 0)
        {
            return;
        }
        Debug.Log("Firing our gun!");
        cooldown = fireRate;
    }
}

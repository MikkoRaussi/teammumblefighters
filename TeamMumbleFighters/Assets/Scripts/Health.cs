﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Health : MonoBehaviour
{

    public float hitPoints = 100f;
    public float currentHitPoints;

    // Start is called before the first frame update
    void Start()
    {
        currentHitPoints = hitPoints;
    }

    [PunRPC]
    public void TakeDamage (float amount)
    {
        currentHitPoints -= amount;

        if (currentHitPoints <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        if (GetComponent<PhotonView>().InstantiationId == 0)
        {
            Destroy(gameObject);
        }
        else
        {
            if (PhotonNetwork.IsMasterClient)
            {
                PhotonNetwork.Destroy(gameObject);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class NetworkManager : MonoBehaviourPunCallbacks
{

    string gameVersion = "1";
    private GameController gameController;

    public Camera standbyCamera;
    public Camera playerCam;
    public bool offlineMode = false;


    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    private void Start()
    {
        gameController = GetComponent<GameController>();
        Connect();
    }

    private void Connect()
    {
        if (offlineMode)
        {
            PhotonNetwork.OfflineMode = true;
            OnJoinedLobby();
        }
        else
        {
            PhotonNetwork.GameVersion = gameVersion;
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    public override void OnConnectedToMaster()
    {
        if (!offlineMode)
        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("OnJoinedLobby");
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        PhotonNetwork.CreateRoom(null, new Photon.Realtime.RoomOptions());
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom");
        SpawnMyPlayer();
    }

    void SpawnMyPlayer()
    {
        GameObject myPlayerGO = PhotonNetwork.Instantiate("Player", Vector3.zero, Quaternion.identity, 0);
        playerCam = Instantiate(playerCam);
        myPlayerGO.GetComponent<PlayerController>().enabled = true;
        myPlayerGO.GetComponent<PlayerController>().playerCam = playerCam;
        playerCam.GetComponent<CameraSmoothFollow>().target = myPlayerGO.transform;
        myPlayerGO.transform.GetChild(0).gameObject.SetActive(true);
        Camera.main.gameObject.SetActive(false);
    }
}

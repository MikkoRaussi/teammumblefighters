﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Sawblade : MonoBehaviour
{
    public float fireRate = 0.5f;
    public float damage = 10f;
    public GameObject player;

    private PhotonView photonView;
    private Animator anim;
    private AudioSource audioData;

    float cooldown = 0;
    bool firing = false;
    bool isLeftGun = false;
    bool isRightGun = false;

    private void Start()
    {
        photonView = player.GetComponent<PhotonView>();
        anim = gameObject.GetComponent<Animator>();
        audioData = GetComponent<AudioSource>();

        if (photonView.IsMine)
        {
            if (transform.parent.name == "Gun_Left")
            {
                isLeftGun = true;
            }
            else
            {
                isRightGun = true;
            }
        }
    }

    void Update()
    {
        cooldown -= Time.deltaTime;
        anim.SetBool("firing", false);
        firing = false;
        if (Input.GetButton("Fire1") && isLeftGun)
        {
            anim.Play("gun_sawblade_spin");
            Fire();
        }

        if (Input.GetButton("Fire2") && isRightGun)
        {
            anim.Play("gun_sawblade_spin");
            Fire();
        }
    }

    void Fire()
    {
        anim.SetBool("firing", true);
        firing = true;
        audioData.Play(0);
        if (cooldown > 0)
        {
            return;
        }
        cooldown = fireRate;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision != null)
        {
            Health h = collision.transform.GetComponent<Health>();

            if (h != null && firing)
            {
                h.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.All, damage);
                //h.TakeDamage(damage);
            }
        }
    }
}
